import { Link, useNavigate } from 'react-router-dom';

import Container from '../ui/container';
import LoginModal from '../LoginModal';
import { Button } from '../ui/button';
import useUserData from '@/hooks/useToken';
import { UserRoleEnum } from '@/utils/interfaces/user';
import useLogout from '@/hooks/useLogout';

const Header: React.FC = () => {
  const navigate = useNavigate();
  const logout = useLogout();
  const { token: isAuthenticated, user } = useUserData();

  return (
    <header
      className="bg-[#cccccc] bg-opacity-50 backdrop-filter backdrop-blur-lg
    fixed top-0 left-0 w-full z-[1]"
    >
      <Container
        className="flex justify-between items-center py-6"
        variant="noPadding"
      >
        <Link to="/">
          <h1 className="text-2xl font-bold text-black">
            Camargo<span className="text-red-500">Play</span>
          </h1>
        </Link>

        <div className="flex items-center gap-4">
          {isAuthenticated ? (
            <>
              {user?.role === UserRoleEnum['ADMIN'] ||
                (user?.role === UserRoleEnum['MASTER'] && (
                  <Button
                    variant="rounded"
                    className="text-black"
                    onClick={() => navigate('/admin')}
                  >
                    Área do Admin
                  </Button>
                ))}
              <Button variant="rounded" className="text-black" onClick={logout}>
                Sair{' '}
              </Button>
            </>
          ) : (
            <LoginModal />
          )}
        </div>
      </Container>
    </header>
  );
};

export default Header;
