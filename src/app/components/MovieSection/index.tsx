import { FC } from 'react';
import MovieCard from '../MovieCard';

import { Link } from 'react-router-dom';
import CircularBadge from '../ui/circular-badge';
import PageTitle from '../ui/page-title';
import { IGenre } from '@/utils/interfaces/genre';

interface MovieSectionProps {
  genre: IGenre;
}

const MovieSection: FC<MovieSectionProps> = ({ genre }: MovieSectionProps) => {
  const lastMovies = genre.movies.slice(0, 5);
  return (
    <section className="sm:py-[20px] xs:py-[18.75px] py-[16.75px] font-nunito">
      <div
        className={`flex flex-row justify-between items-center sm:mb-6 mb-[22.75px]`}
      >
        <PageTitle title={genre.name} />
        <CircularBadge as={Link} to={`/${encodeURIComponent(genre.id)}`}>
          Ver mais
        </CircularBadge>
      </div>
      <div className="xs:min-h-[250px] min-h-[216px]">
        <div className="flex flex-row overflow-x-scroll sm:overflow-x-hidden scrollbar-hide p-3 sm:gap-4 gap-2">
          {lastMovies?.map((movie) => (
            <MovieCard
              key={movie.id}
              id={movie.id}
              title={movie.title}
              genreId={genre.id}
              banner={movie.image}
            />
          ))}
        </div>
      </div>
    </section>
  );
};

export default MovieSection;
