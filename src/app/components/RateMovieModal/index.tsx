import React from 'react';
import Rate from 'rc-rate';
import 'rc-rate/assets/index.css';

import {
  Dialog,
  DialogContent,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from '../ui/dialog';
import { Button } from '../ui/button';
import { api } from '@/services/api';
import { toast } from 'react-toastify';
import { IMovie } from '@/utils/interfaces/movie';
import useUserData from '@/hooks/useToken';
import { UserRoleEnum } from '@/utils/interfaces/user';

interface IRateMovieModalProps {
  movie?: IMovie;
  revalidate: boolean;
  setRevalidate: React.Dispatch<React.SetStateAction<boolean>>;
}

const RateMovieModal: React.FC<IRateMovieModalProps> = ({
  movie,
  revalidate,
  setRevalidate,
}) => {
  const userData = useUserData();
  const [rate, setRate] = React.useState<number>(0);
  const [open, setOpen] = React.useState(false);
  const onSubmit = async () => {
    await api.post(`/movies/${movie?.id}/rate`, {
      rate,
    });

    toast.success('Registro realizado com sucesso!');
    setRevalidate && setRevalidate(!revalidate ?? false);
    setOpen && setOpen(false);
  };

  return (
    <Dialog
      open={open}
      onOpenChange={(newOpen) => {
        setOpen(newOpen);
      }}
    >
      <DialogTrigger>
        {userData?.user?.id &&
          userData?.user?.role === UserRoleEnum.USER &&
          movie?.ratings?.find(
            (rating) => rating?.user?.id === userData?.user?.id
          ) === undefined && (
            <Button className="border-white" variant="rounded">
              Avaliar
            </Button>
          )}
      </DialogTrigger>
      <DialogContent>
        <DialogHeader className="mb-4">
          <DialogTitle>
            Deseja avaliar o filme <strong>{movie?.title}</strong>?
            <br />
            <br />
            <div className="flex justify-center">
              <Rate
                style={{ fontSize: '42px' }}
                count={4}
                value={rate}
                onChange={(e) => setRate(e)}
              />
            </div>
          </DialogTitle>
        </DialogHeader>
        <Button
          variant="rounded"
          className="mx-2"
          onClick={() => setOpen && setOpen(false)}
        >
          Cancelar
        </Button>
        <Button variant="default" onClick={onSubmit}>
          Sim
        </Button>
      </DialogContent>
    </Dialog>
  );
};

export default RateMovieModal;
