import { cn } from '@/lib/utils';
import { VariantProps, cva } from 'class-variance-authority';
import React from 'react';

const containerVariants = cva('max-w-[80vw] xs:mt-4 mt-2 ml-auto mr-auto', {
  variants: {
    variant: {
      default: 'bg-transparent lg:py-12 md:py-8 sm:py-6',
      noPadding: 'bg-transparent',
    },
  },
  defaultVariants: {
    variant: 'default',
  },
});

interface ContainerProps
  extends React.HTMLAttributes<HTMLDivElement>,
    VariantProps<typeof containerVariants> {
  children: React.ReactNode;
}

const Container: React.FC<ContainerProps> = ({
  children,
  className,
  variant,
}) => {
  return (
    <div className={cn(containerVariants({ variant, className }))}>
      {children}
    </div>
  );
};

export default Container;
