import { Formik, Form as Forms } from 'formik';
import * as Yup from 'yup';
import React from 'react';
import { toast } from 'react-toastify';

interface FormProps<T> {
  initialValues: T;
  onSubmit: (values: T) => Promise<void>;
  validationSchema?: Yup.ObjectSchema<any>;
  children: React.ReactNode;
  submitButton: React.ReactElement;
  cancelButton?: React.ReactElement;
}

const Form = <T extends Record<string, any>>({
  initialValues,
  onSubmit,
  validationSchema,
  children,
  submitButton,
  cancelButton,
}: FormProps<T>) => {
  return (
    <Formik
      initialValues={initialValues}
      enableReinitialize
      validationSchema={validationSchema}
      onSubmit={async (values, { setSubmitting }) => {
        try {
          await onSubmit(values);
        } catch (error: any) {
          console.log(error);
          toast.error(error?.response?.data?.error || 'Erro inesperado');
        } finally {
          setSubmitting(false);
        }
      }}
    >
      {({ isSubmitting, handleSubmit }) => (
        <Forms
          className="w-[80%] flex flex-col gap-1 mx-auto"
          onSubmit={(e) => {
            e.preventDefault();
            handleSubmit();
          }}
        >
          {children}
          <div className="flex flex-row justify-around">
            {cancelButton}
            {React.cloneElement(submitButton, {
              type: 'submit',
              disabled: isSubmitting,
            })}
          </div>
        </Forms>
      )}
    </Formik>
  );
};

export default Form;
