import { Button } from './button';

const createPageButton = (
  page: number,
  currentPage: number,
  onClick: (page: number) => void
) => (
  <Button
    className="h-8 w-8 p-0"
    variant={page === currentPage ? 'default' : 'outline'}
    disabled={page === currentPage}
    onClick={() => onClick(page)}
  >
    {page + 1}
  </Button>
);

const paginationsButtons = (
  currentPage: number,
  totalPages: number,
  onClick: (page: number) => void
) => {
  const paginationItems = [];

  paginationItems.push(createPageButton(0, currentPage, onClick));

  if (totalPages <= 5) {
    for (let i = 1; i < totalPages; i++) {
      paginationItems.push(createPageButton(i, currentPage, onClick));
    }
  } else {
    let startPage = currentPage - 2;
    let endPage = currentPage + 2;

    if (currentPage < 4) {
      startPage = 1;
      endPage = 4;
    } else if (currentPage > totalPages - 5) {
      startPage = totalPages - 5;
      endPage = totalPages - 2;
    }

    if (startPage > 1) {
      paginationItems.push(
        <Button
          disabled
          className="h-8 w-8 p-0"
          variant="outline"
          key={startPage - 1}
        >
          ...
        </Button>
      );
    }

    for (let i = startPage; i <= endPage; i++) {
      paginationItems.push(createPageButton(i, currentPage, onClick));
    }

    if (endPage < totalPages - 2) {
      paginationItems.push(
        <Button
          disabled
          className="h-8 w-8 p-0"
          variant="outline"
          key={endPage + 1}
        >
          ...
        </Button>
      );
    }
    paginationItems.push(
      createPageButton(totalPages - 1, currentPage, onClick)
    );
  }

  return paginationItems;
};

export default paginationsButtons;
