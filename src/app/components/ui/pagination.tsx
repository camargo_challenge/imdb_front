'use client';

import { ChevronLeftIcon, ChevronRightIcon } from '@radix-ui/react-icons';

import { AnimatePresence, motion } from 'framer-motion';
import paginationsButtons from './pagination-buttons';
import { Button } from './button';

interface TablePaginationProps {
  page: number;
  setPage: (page: number) => void;
  totalPages: number;
}

function TablePagination({ page, setPage, totalPages }: TablePaginationProps) {
  const paginationItems = paginationsButtons(page, totalPages, setPage);

  return (
    <div className="flex w-full items-center px-2 pt-10 pb-4 justify-center relative">
      <div className="flex items-center">
        <div className="flex items-center gap-1">
          <AnimatePresence initial={false}>
            <div className="flex items-center justify-center text-sm font-medium gap-1">
              <motion.div
                initial={{ opacity: 0, x: -10 }}
                animate={{ opacity: 1, x: 0 }}
                exit={{ opacity: 0, x: 10 }}
                transition={{ duration: 0.3 }}
              >
                <Button
                  variant="outline"
                  className="h-8 w-8 p-0"
                  onClick={() => setPage(page - 1)}
                  disabled={page === 0}
                >
                  <ChevronLeftIcon className="h-4 w-4" />
                </Button>
              </motion.div>
              <div className="flex items-center justify-center text-sm font-medium gap-1">
                {paginationItems.map((item, index) => (
                  <motion.div
                    key={index}
                    initial={{ opacity: 0, x: -10 }}
                    animate={{ opacity: 1, x: 0 }}
                    exit={{ opacity: 0, x: 10 }}
                    transition={{ duration: 0.3 }}
                  >
                    {item}
                  </motion.div>
                ))}
              </div>
            </div>
            <motion.div
              initial={{ opacity: 0, x: -10 }}
              animate={{ opacity: 1, x: 0 }}
              exit={{ opacity: 0, x: 10 }}
              transition={{ duration: 0.3 }}
            >
              <Button
                variant="outline"
                className="h-8 w-8 p-0"
                onClick={() => page + 1 < totalPages && setPage(page + 1)}
                disabled={page + 1 >= totalPages}
              >
                <ChevronRightIcon className="h-4 w-4" />
              </Button>
            </motion.div>
          </AnimatePresence>
        </div>
      </div>
    </div>
  );
}

export default TablePagination;
