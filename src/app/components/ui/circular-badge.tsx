import React from 'react';

import { cn } from '@/lib/utils';
import { VariantProps, cva } from 'class-variance-authority';

const badgeVariants = cva(
  'sm:py-1 py-[2px] sm:text-[14px] xs:text-[12.75px] text-[12px] sm:px-4 px-3 rounded-full hover:-translate-y-1 transition-all duration-300  cursor-pointer border ',
  {
    variants: {
      variant: {
        default:
          'bg-gray-50 hover:bg-gray-100 border-black hover:border-gray-300',
        white: 'bg-white border-white hover:bg-gray-10',
        transparent: 'bg-transparent border-black hover:bg-gray-10 shadow-none',
      },
    },
    defaultVariants: {
      variant: 'default',
    },
  }
);

interface CircularBadgeProps
  extends React.HTMLAttributes<HTMLDivElement>,
    VariantProps<typeof badgeVariants> {
  as?: React.ElementType;
  to?: string;
  children: React.ReactNode;
}

const CircularBadge: React.FC<CircularBadgeProps> = React.memo(
  ({ as: Element, children, className, variant, ...rest }) => {
    if (Element) {
      return (
        <Element
          className={cn(badgeVariants({ variant, className }))}
          {...rest}
        >
          {children}
        </Element>
      );
    }

    return (
      <div className={cn(badgeVariants({ variant, className }))} {...rest}>
        {children}
      </div>
    );
  }
);

export default CircularBadge;
