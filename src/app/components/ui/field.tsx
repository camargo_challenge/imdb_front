import { ErrorMessage, Field as FormikField } from 'formik';
import { Input } from './input';

interface FieldProps extends React.InputHTMLAttributes<HTMLInputElement> {
  name: string;
  label: string;
  value: string;
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
  id: string;
  labelColor?: string;
  password?: boolean;
  type?: string;
  mask?: string;
  disabled?: boolean;
  labelWeight?: 500 | 700;
  variant?: string;
}

function Field({
  name,
  label,
  value,
  onChange,
  id,
  type = 'text',
  disabled,
  variant,
  ...props
}: FieldProps) {
  return (
    <>
      <FormikField
        as={Input}
        name={name}
        label={label}
        value={value}
        onChange={onChange}
        type={type}
        id={id}
        disabled={disabled}
        variant={variant}
        {...props}
      />
      <div className="h-4 mt-1">
        <ErrorMessage
          name={name}
          component="div"
          className="text-red-500 text-xs"
        />
      </div>
    </>
  );
}

export default Field;
