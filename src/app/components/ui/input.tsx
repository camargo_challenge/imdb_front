import * as React from 'react';
import Select, { GroupBase, OptionsOrGroups } from 'react-select';

import { cn } from '@/lib/utils';
import { Label } from './label';
import { selectStyles } from '@/app/styles/common';
import { api } from '@/services/api';

export interface InputProps
  extends React.InputHTMLAttributes<HTMLInputElement> {
  label?: string;
  options?: OptionsOrGroups<
    {
      value: string;
      label: string;
    },
    GroupBase<{
      value: string;
      label: string;
    }>
  >;
}
// se o tipo for file e o value for uma string, então é uma url, deve aparecer um preview da imagem e um botão para remover a imagem. Ao remover a imagem, o value deve ser undefined e portanto o input deve voltar a ser do tipo file

const Input = React.forwardRef<HTMLInputElement, InputProps>(
  ({ className, type, label, options, ...props }, ref) => {
    return (
      <>
        <Label htmlFor={props.name} className={`text-black text-sm font-[500]`}>
          {label}
        </Label>
        {type === 'select' && options ? (
          <Select
            options={options}
            styles={selectStyles}
            onChange={(e) =>
              props.onChange?.({
                target: { name: props.name, value: e?.value },
              } as any)
            }
            value={{ label: props.value, value: props.value }}
          />
        ) : type === 'file' && props.value !== '' && props.value !== null ? (
          <div className="flex items-center space-x-2">
            <input
              ref={ref}
              type="text"
              className={cn(
                'border border-gray-300 rounded-md w-full px-3 py-2 text-sm focus:outline-none focus:ring-1 focus:ring-gray-300',
                className
              )}
              {...props}
            />
            {props.value && (
              <button
                type="button"
                className="text-red-500 hover:text-red-600"
                onClick={() =>
                  props.onChange?.({
                    target: { name: props.name, value: '' },
                  } as any)
                }
              >
                Remover
              </button>
            )}
          </div>
        ) : (
          <input
            type={type}
            className={cn(
              'flex h-9 w-full border border-primary bg-transparent px-3 py-1 text-sm shadow-sm transition-colors file:border-0 file:bg-transparent file:text-sm file:font-medium placeholder:text-muted-foreground focus-visible:outline-none focus-visible:ring-1 focus-visible:ring-ring disabled:cursor-not-allowed disabled:opacity-50 rounded-full',
              className
            )}
            ref={ref}
            {...props}
            {...(type === 'file' &&
              props.onChange && {
                onChange: async (e: React.ChangeEvent<HTMLInputElement>) => {
                  const data = new FormData();
                  data.append('file', e.target.files?.[0] as Blob);
                  const response = await api.post('/upload-file', data, {
                    headers: {
                      'Content-Type': 'multipart/form-data',
                    },
                  });

                  return (
                    props.onChange &&
                    props?.onChange({
                      target: {
                        name: props.name,
                        value: response.data.file.location,
                      },
                    } as any)
                  );
                },
              })}
          />
        )}
      </>
    );
  }
);
Input.displayName = 'Input';

export { Input };
