import React from 'react';

interface PageTitleProps {
  title: string;
}

const PageTitle: React.FC<PageTitleProps> = ({ title }) => {
  return (
    <h3 className="sm:text-[22.25px] xs:text-[20px] text-[18.75px] dark:text-gray-50 sm:font-bold font-semibold relative w-fit">
      <span>{title}</span>
      <div className="absolute -bottom-1 left-0 w-1/2 h-1 bg-red-500 rounded-full"></div>
    </h3>
  );
};

export default PageTitle;
