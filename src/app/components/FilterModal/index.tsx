import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import Select, { MultiValue } from 'react-select';

import { Button } from '../ui/button';
import CircularBadge from '../ui/circular-badge';
import {
  Dialog,
  DialogContent,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from '../ui/dialog';
import { Label } from '../ui/label';
import { toast } from 'react-toastify';
import { api } from '@/services/api';
import { selectStyles } from '@/app/styles/common';
import FilterContext, { SelectOption } from '@/context/FilterContext';

interface FetchFunction {
  (): Promise<any[]>;
}

const useFetchData = (fetchFunction: FetchFunction) => {
  const [data, setData] = useState<SelectOption[]>([]);

  useEffect(() => {
    fetchFunction()
      .then((result) => {
        setData(
          result?.map((item) => ({
            value: item?.id || item,
            label: item?.name || item,
          })) || []
        );
      })
      .catch((err) => {
        toast.error(err.response?.data.message ?? 'Erro ao buscar dados');
      });
  }, []);

  return data;
};

const MultiSelect = ({
  options,
  value,
  onChange,
}: {
  options: SelectOption[];
  value?: SelectOption[];
  onChange?: (e: MultiValue<SelectOption>) => void;
}) => (
  <Select
    options={options}
    isMulti
    styles={selectStyles}
    onChange={onChange}
    value={value}
  />
);

const FilterModal: React.FC = () => {
  const navigate = useNavigate();
  const [open, setOpen] = useState(false);
  const { filterData, setFilterData } = React.useContext(FilterContext);

  const genres = useFetchData(() =>
    api.get('/genres?onlyWithMovies=true').then((res) => res.data)
  );
  const directors = useFetchData(() =>
    api.get('/movies/directors').then((res) => res.data)
  );
  const actors = useFetchData(() => api.get('/actors').then((res) => res.data));

  return (
    <Dialog open={open} onOpenChange={(newOpen) => setOpen(newOpen)}>
      <DialogTrigger>
        <CircularBadge as={Button} variant="transparent" className="text-black">
          Filtros
        </CircularBadge>
      </DialogTrigger>
      <DialogContent>
        <DialogHeader>
          <DialogTitle>Filtros</DialogTitle>
        </DialogHeader>
        <Label className="text-black mt-4">Gênero</Label>
        <MultiSelect
          options={genres}
          value={filterData?.genres}
          onChange={(e) => {
            setFilterData({
              ...filterData,
              genres: e,
            });
          }}
        />
        <Label className="text-black">Diretor</Label>
        <MultiSelect
          options={directors}
          value={filterData?.directors}
          onChange={(e) => {
            setFilterData({
              ...filterData,
              directors: e,
            });
          }}
        />
        <Label className="text-black">Atores</Label>
        <MultiSelect
          options={actors}
          value={filterData?.actors}
          onChange={(e) => {
            setFilterData({
              ...filterData,
              actors: e,
            });
          }}
        />
        <div className="flex justify-around items-center mt-4">
          <Button
            variant="rounded"
            onClick={() => {
              setOpen(false);
              setFilterData({
                ...filterData,
                actors: [],
                directors: [],
                genres: [],
                search: '',
                requested: !filterData?.requested,
              });
            }}
          >
            Limpar
          </Button>
          <Button
            variant="default"
            onClick={() => {
              setOpen(false);
              navigate('/search');
              setFilterData({
                ...filterData,
                requested: !filterData?.requested,
              });
            }}
          >
            Pesquisar
          </Button>
        </div>
      </DialogContent>
    </Dialog>
  );
};

export default FilterModal;
