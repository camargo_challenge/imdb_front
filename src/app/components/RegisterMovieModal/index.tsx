import React from 'react';
import {
  Dialog,
  DialogContent,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from '../ui/dialog';
import { Button } from '../ui/button';
import RegisterForm from './RegisterMovieForm';
import { IRegisterMovie } from './RegisterMovieForm/interface';

interface IRegisterMovieModalProps {
  isEdit?: boolean;
  movie?: IRegisterMovie;
  revalidate?: boolean;
  setRevalidate?: React.Dispatch<React.SetStateAction<boolean>>;
}

const RegisterMovieModal: React.FC<IRegisterMovieModalProps> = ({
  isEdit = false,
  movie,
  revalidate,
  setRevalidate,
}) => {
  const [open, setOpen] = React.useState(false);
  return (
    <Dialog
      open={open}
      onOpenChange={(newOpen) => {
        setOpen(newOpen);
      }}
    >
      <DialogTrigger>
        <Button variant="default">{isEdit ? 'Editar' : 'Cadastrar'}</Button>
      </DialogTrigger>
      <DialogContent>
        <DialogHeader className="mb-4">
          <DialogTitle>
            Deseja {isEdit ? 'editar' : 'cadastrar'} um filme?
          </DialogTitle>
        </DialogHeader>
        <RegisterForm
          movie={movie}
          setOpen={setOpen}
          revalidate={revalidate}
          setRevalidate={setRevalidate}
        />
      </DialogContent>
    </Dialog>
  );
};

export default RegisterMovieModal;
