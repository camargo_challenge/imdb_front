export const registerMovieInputs = [
  {
    name: 'title',
    label: 'Título',
    placeholder: 'Avengers',
    type: 'text',
  },
  {
    name: 'director',
    label: 'Diretor',
    placeholder: 'Diretor',
    type: 'text',
  },
  {
    name: 'synopsis',
    label: 'Sinopse',
    placeholder: 'Sinopse',
    type: 'textarea',
    maxLength: 250,
  },
  {
    name: 'releaseDate',
    label: 'Data de lançamento',
    placeholder: 'Data de lançamento',
    type: 'date',
  },
  {
    name: 'duration',
    label: 'Duração',
    placeholder: 'Duração',
    type: 'number',
  },
  {
    name: 'classification',
    label: 'Classificação',
    placeholder: 'Classificação',
    type: 'select',
    options: [
      { value: 'Livre', label: 'Livre' },
      { value: '10 anos', label: '10 anos' },
      { value: '12 anos', label: '12 anos' },
      { value: '14 anos', label: '14 anos' },
      { value: '16 anos', label: '16 anos' },
      { value: '18 anos', label: '18 anos' },
    ],
  },
  {
    name: 'image',
    label: 'Imagem',
    placeholder: 'Imagem',
    type: 'file',
    accept: 'image/*',
    // ao alterar envia o arquivo para o servidor, que retorna a url da imagem
    // para ser salva no banco de dados
    /*     onChange: async (e: React.ChangeEvent<HTMLInputElement>) => {
      const data = new FormData();
      data.append('file', e.target.files?.[0] as Blob);
      const response = await fetch('http://localhost:3001/upload', {
        method: 'POST',
        body: data,
      });
      const json = await response.json();
      return json.url;
    }, */
  },
];
