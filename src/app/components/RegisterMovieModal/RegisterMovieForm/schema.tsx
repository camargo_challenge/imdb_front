import * as Yup from 'yup';

const registerMovieValidationSchema = Yup.object().shape({
  title: Yup.string().required('Título é obrigatório').trim().max(250).min(2),
  director: Yup.string()
    .required('Diretor é obrigatório')
    .trim()
    .max(250)
    .min(2),
  synopsis: Yup.string()
    .required('Sinopse é obrigatório')
    .trim()
    .max(250)
    .min(2),
  releaseDate: Yup.string()
    .required('Data de lançamento é obrigatório')
    .trim()
    .max(250)
    .min(2),
  duration: Yup.string()
    .required('Duração é obrigatório')
    .trim()
    .max(250)
    .min(2),
  classification: Yup.string()
    .required('Classificação é obrigatório')
    .oneOf(['Livre', '10 anos', '12 anos', '14 anos', '16 anos', '18 anos']),
  image: Yup.string().required('Imagem é obrigatório').trim().max(250).min(2),
});

export default registerMovieValidationSchema;
