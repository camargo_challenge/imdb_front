import React from 'react';
import CreatableSelect from 'react-select/creatable';
import Select from 'react-select';
import { toast } from 'react-toastify';

import Form from '../../ui/form';
import Field from '../../ui/field';
import { Button } from '../../ui/button';
import { registerMovieInputs } from './inputs';
import { IRegisterMovie } from './interface';
import registerMovieValidationSchema from './schema';
import { selectStyles } from '@/app/styles/common';
import { api } from '@/services/api';
import { SelectOption } from '@/context/FilterContext';
import { IActor } from '@/utils/interfaces/actors';
import { Label } from '@radix-ui/react-label';
import { IGenre } from '@/utils/interfaces/genre';

interface IRegisterFormProps {
  movie?: IRegisterMovie;
  setOpen?: React.Dispatch<React.SetStateAction<boolean>>;
  revalidate?: boolean;
  setRevalidate?: React.Dispatch<React.SetStateAction<boolean>>;
}

const RegisterForm: React.FC<IRegisterFormProps> = ({
  movie,
  setOpen,
  revalidate,
  setRevalidate,
}) => {
  const [actors, setActors] = React.useState<SelectOption[]>([]);
  const [genres, setGenres] = React.useState<any>([]);
  const [formValues, setFormValues] = React.useState<IRegisterMovie>(
    movie ?? {
      title: '',
      director: '',
      genre: null,
      actors: [],
      synopsis: '',
      releaseDate: new Date(),
      duration: '',
      classification: 'Livre',
      image: '',
    }
  );

  React.useEffect(() => {
    const fetchActors = async () => {
      const response = await api.get('/actors');
      return response.data as IActor[];
    };

    const fetchGenres = async () => {
      const response = await api.get('/genres');
      return response.data as IGenre[];
    };

    fetchActors()
      .then((data) => {
        setActors(
          data?.map((actor) => ({
            value: actor.id,
            label: actor.name,
          }))
        );
      })
      .catch((err) => {
        toast.error(err.response?.data.message ?? 'Erro ao buscar atores');
      });

    fetchGenres()
      .then((data) => {
        setGenres(
          data?.map((genre) => ({
            value: genre.id,
            label: genre.name,
          }))
        );
      })
      .catch((err) => {
        toast.error(err.response?.data.message ?? 'Erro ao buscar gêneros');
      });
  }, []);

  const onSubmit = async (values: IRegisterMovie) => {
    await api.post('/movies', {
      ...values,
      actors: values.actors.map((actor) =>
        actor?.__isNew__ ? { name: actor.label } : { id: actor.value }
      ),
      genre: { id: values.genre?.value },
    });

    toast.success('Filme cadastrado com sucesso!');
    setRevalidate && setRevalidate(!revalidate);
    setOpen && setOpen(false);
  };

  const onEdit = async (values: IRegisterMovie) => {
    await api.patch(`/movies/${movie?.id}`, {
      ...values,
      actors: values.actors.map((actor) =>
        actor?.__isNew__ ? { name: actor.label } : { id: actor.value }
      ),
    });

    toast.success('Filme editado com sucesso!');
    setRevalidate && setRevalidate(!revalidate);
    setOpen && setOpen(false);
  };

  return (
    <div className="max-h-[70vh] overflow-auto">
      <Form
        initialValues={formValues}
        validationSchema={registerMovieValidationSchema}
        onSubmit={movie ? onEdit : onSubmit}
        submitButton={<Button variant="default">Salvar</Button>}
        cancelButton={
          <Button variant="rounded" onClick={() => setOpen && setOpen(false)}>
            Cancelar
          </Button>
        }
      >
        {registerMovieInputs.map((input) => (
          <Field
            key={input.name}
            value={formValues[input.name as keyof typeof formValues] as string}
            onChange={(e) =>
              setFormValues({ ...formValues, [input.name]: e.target.value })
            }
            id={input.name}
            {...input}
          />
        ))}
        <Label className="text-black text-sm font-[500]">Atores</Label>
        <CreatableSelect
          isMulti
          options={actors}
          styles={selectStyles}
          onChange={(e) => setFormValues({ ...formValues, actors: e })}
          value={formValues.actors}
        />
        <Label className="text-black text-sm font-[500] my-2">Gênero</Label>
        <Select
          options={genres}
          styles={selectStyles}
          onChange={(e) => setFormValues({ ...formValues, genre: e })}
          value={formValues.genre}
        />
      </Form>
    </div>
  );
};

export default RegisterForm;
