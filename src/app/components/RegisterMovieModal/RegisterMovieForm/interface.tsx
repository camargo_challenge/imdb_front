import { SelectOption } from '@/context/FilterContext';
import { IGenre } from '@/utils/interfaces/genre';
import { MultiValue } from 'react-select';

export interface IRegisterMovie {
  id?: string;
  title: string;
  director: string;
  synopsis: string;
  releaseDate: Date;
  duration: string;
  classification:
    | 'Livre'
    | '10 anos'
    | '12 anos'
    | '14 anos'
    | '16 anos'
    | '18 anos';
  image: string;
  genre: IGenre | null;
  actors: MultiValue<SelectOption>;
}
