import { IActor } from '@/utils/interfaces/actors';
import CircularBadge from '../ui/circular-badge';

const ActorsSection = ({ actors }: { actors: IActor[] }) => {
  return (
    <div className="flex flex-row overflow-x-auto gap-4 p-2">
      {actors.map((actor) => (
        <CircularBadge
          key={actor.id}
          variant="transparent"
          className="border-white"
        >
          {actor.name}
        </CircularBadge>
      ))}
    </div>
  );
};

export default ActorsSection;
