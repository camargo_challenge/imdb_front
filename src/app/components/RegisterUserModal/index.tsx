import React from 'react';
import {
  Dialog,
  DialogContent,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from '../ui/dialog';
import { Button } from '../ui/button';
import RegisterForm from './RegisterUserForm';
import { IRegisterUser } from './RegisterUserForm/interface';

interface IRegisterUserModalProps {
  isEdit?: boolean;
  user?: IRegisterUser;
  revalidate: boolean;
  setRevalidate: React.Dispatch<React.SetStateAction<boolean>>;
}

const RegisterUserModal: React.FC<IRegisterUserModalProps> = ({
  isEdit = false,
  user,
  revalidate,
  setRevalidate,
}) => {
  const [open, setOpen] = React.useState(false);
  return (
    <Dialog
      open={open}
      onOpenChange={(newOpen) => {
        setOpen(newOpen);
      }}
    >
      <DialogTrigger>
        <Button variant="default">{isEdit ? 'Editar' : 'Cadastrar'}</Button>
      </DialogTrigger>
      <DialogContent>
        <DialogHeader className="mb-4">
          <DialogTitle>
            Deseja {isEdit ? 'editar' : 'cadastrar'} um usuário?
          </DialogTitle>
        </DialogHeader>
        <RegisterForm
          user={user}
          setOpen={setOpen}
          setRevalidate={setRevalidate}
          revalidate={revalidate}
        />
      </DialogContent>
    </Dialog>
  );
};

export default RegisterUserModal;
