import { UserRoleEnum } from '@/utils/interfaces/user';

export interface IRegisterUser {
  id?: string;
  name: string;
  email: string;
  password: string;
  passwordConfirmation: string;
  role: UserRoleEnum;
  image: string;
}
