import React from 'react';
import Form from '../../ui/form';
import Field from '../../ui/field';
import { Button } from '../../ui/button';
import { toast } from 'react-toastify';
import { registerUserInputs } from './inputs';
import { IRegisterUser } from './interface';
import registerUserValidationSchema from './schema';
import { api } from '@/services/api';
import { UserRoleEnum } from '@/utils/interfaces/user';

interface IRegisterFormProps {
  user?: IRegisterUser;
  setOpen?: React.Dispatch<React.SetStateAction<boolean>>;
  setRevalidate?: React.Dispatch<React.SetStateAction<boolean>>;
  revalidate?: boolean;
}

const RegisterForm: React.FC<IRegisterFormProps> = ({
  user,
  setOpen,
  setRevalidate,
  revalidate,
}) => {
  const [formValues, setFormValues] = React.useState<IRegisterUser>(
    user ?? {
      name: '',
      email: '',
      password: '',
      passwordConfirmation: '',
      role: UserRoleEnum['USER'],
      image: '',
    }
  );

  const onSubmit = async (values: IRegisterUser) => {
    await api.post('/users', values);

    toast.success('Registro realizado com sucesso!');
    setRevalidate && setRevalidate(!revalidate ?? false);
    setOpen && setOpen(false);
  };

  const onEdit = async () => {
    await api.patch(`/users/${user?.id}`, formValues);

    toast.success('Registro atualizado com sucesso!');
    setRevalidate && setRevalidate(!revalidate ?? false);
    setOpen && setOpen(false);
  };

  return (
    <Form
      initialValues={formValues}
      validationSchema={registerUserValidationSchema}
      onSubmit={user ? onEdit : onSubmit}
      submitButton={<Button variant="default">Salvar</Button>}
      cancelButton={
        <Button variant="rounded" onClick={() => setOpen && setOpen(false)}>
          Cancelar
        </Button>
      }
    >
      {registerUserInputs.map((input) => (
        <Field
          key={input.name}
          value={formValues[input.name as keyof typeof formValues] as string}
          onChange={(e) =>
            setFormValues({ ...formValues, [input.name]: e.target.value })
          }
          id={input.name}
          {...input}
        />
      ))}
    </Form>
  );
};

export default RegisterForm;
