export const registerUserInputs = [
  {
    name: 'name',
    label: 'Name',
    placeholder: 'John Doe',
    type: 'text',
  },
  {
    name: 'email',
    label: 'Email',
    placeholder: 'Email',
    type: 'email',
  },
  {
    name: 'password',
    label: 'Senha',
    placeholder: 'Senha',
    type: 'password',
  },
  {
    name: 'passwordConfirmation',
    label: 'Confirme a senha',
    placeholder: 'Confirme a senha',
    type: 'password',
  },
  {
    name: 'role',
    label: 'Função',
    placeholder: 'Função',
    type: 'select',
    options: [
      { value: 'USER', label: 'USER' },
      { value: 'ADMIN', label: 'ADMIN' },
    ],
  },
  {
    name: 'image',
    label: 'Imagem',
    placeholder: 'Imagem',
    type: 'file',
    accept: 'image/*',
  },
];
