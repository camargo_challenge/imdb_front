import * as Yup from 'yup';

const registerUserValidationSchema = Yup.object().shape({
  name: Yup.string()
    .min(2, 'O nome deve ter pelo menos 2 caracteres')
    .trim()
    .required('O nome é obrigatório'),
  email: Yup.string()
    .email('Endereço de email inválido')
    .required('O email é obrigatório'),
  password: Yup.string()
    .min(8, 'A senha deve ter pelo menos 8 caracteres')
    .matches(
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,255})/,
      'A senha deve conter pelo menos uma letra maiúscula, uma letra minúscula e um número'
    )
    .required('A senha é obrigatória'),
  passwordConfirmation: Yup.string().oneOf(
    [Yup.ref('password')],
    'As senhas devem ser iguais'
  ),
  role: Yup.string().required('O papel é obrigatório').oneOf(['USER', 'ADMIN']),
});

export default registerUserValidationSchema;
