import React, { useContext } from 'react';
import { toast } from 'react-toastify';

import Form from '../../ui/form';
import Field from '../../ui/field';
import { Button } from '../../ui/button';
import { api } from '@/services/api';
import loginValidationSchema from './schema';
import UserContext, { UserContextProps } from '@/context/UserContext';

interface LoginFormProps {
  setOpen: React.Dispatch<React.SetStateAction<boolean>>;
}

const LoginForm: React.FC<LoginFormProps> = ({ setOpen }) => {
  const { setUserData } = useContext(UserContext) as UserContextProps;
  const [formValues, setFormValues] = React.useState({
    email: '',
    password: '',
  });

  const onSubmit = async () => {
    const response = await api.post('/auth/login', formValues);

    delete response?.data?.user?.email;

    setUserData(response.data);

    toast.success('Login realizado com sucesso');
    setOpen(false);

    return response.data;
  };

  return (
    <Form
      initialValues={formValues}
      onSubmit={onSubmit}
      validationSchema={loginValidationSchema}
      submitButton={<Button variant="default">Login</Button>}
      cancelButton={<Button variant="rounded">Cancelar</Button>}
    >
      <Field
        name="email"
        label="Email"
        value={formValues.email}
        onChange={(e) =>
          setFormValues({ ...formValues, email: e.target.value })
        }
        id="email"
      />
      <Field
        name="password"
        label="Senha"
        type="password"
        value={formValues.password}
        onChange={(e) =>
          setFormValues({ ...formValues, password: e.target.value })
        }
        id="password"
      />
    </Form>
  );
};

export default LoginForm;
