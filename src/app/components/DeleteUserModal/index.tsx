import React from 'react';
import {
  Dialog,
  DialogContent,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from '../ui/dialog';
import { Button } from '../ui/button';
import { IUser, UserStatusEnum } from '@/utils/interfaces/user';
import { api } from '@/services/api';
import { toast } from 'react-toastify';

interface IDisableUserModalProps {
  user: IUser;
  revalidate: boolean;
  setRevalidate: React.Dispatch<React.SetStateAction<boolean>>;
}

const DisableUserModal: React.FC<IDisableUserModalProps> = ({
  user,
  revalidate,
  setRevalidate,
}) => {
  const [open, setOpen] = React.useState(false);
  const onSubmit = async () => {
    await api.delete(`/users/${user?.id}`);

    toast.success('Registro realizado com sucesso!');
    setRevalidate && setRevalidate(!revalidate ?? false);
    setOpen && setOpen(false);
  };

  return (
    <Dialog
      open={open}
      onOpenChange={(newOpen) => {
        setOpen(newOpen);
      }}
    >
      <DialogTrigger>
        <Button variant="rounded" className="ml-2">
          {user?.status !== UserStatusEnum['ativo'] ? 'Reativar' : 'Excluir'}
        </Button>
      </DialogTrigger>
      <DialogContent>
        <DialogHeader className="mb-4">
          <DialogTitle>
            Deseja{' '}
            {user?.status !== UserStatusEnum['ativo'] ? 'ativar' : 'desativar'}{' '}
            o usuário?
          </DialogTitle>
        </DialogHeader>
        <Button
          variant="rounded"
          className="mx-2"
          onClick={() => setOpen && setOpen(false)}
        >
          Cancelar
        </Button>
        <Button variant="default" onClick={onSubmit}>
          Sim
        </Button>
      </DialogContent>
    </Dialog>
  );
};

export default DisableUserModal;
