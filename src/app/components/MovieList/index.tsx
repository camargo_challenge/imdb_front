import React from 'react';
import MovieCard from '../MovieCard';
import { IMovie } from '@/utils/interfaces/movie';

interface MovieListProps {
  data: IMovie[];
  genreId: string;
}

const MovieList: React.FC<MovieListProps> = ({ data, genreId }) => {
  return (
    <ul className="flex flex-wrap start gap-4 my-4">
      {data && data?.map((movie) => (
        <li key={movie.id} className="w-[250px] h-[300px]">
          <MovieCard
            id={movie.id}
            title={movie.title}
            banner={movie.image}
            genreId={genreId}
          />
        </li>
      ))}
    </ul>
  );
};

export default MovieList;
