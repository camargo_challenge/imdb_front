import React from 'react';
import { useNavigate } from 'react-router-dom';

import { Input } from '@/app/components/ui/input';
import { Button } from '../ui/button';
import FilterModal from '../FilterModal';
import FilterContext from '@/context/FilterContext';

const SearchBar: React.FC = () => {
  const navigate = useNavigate();
  const { filterData, setFilterData } = React.useContext(FilterContext);

  return (
    <div className="flex w-full sm:max-w-[50%] items-center space-x-2 mx-auto">
      <Input
        type="text"
        placeholder="Pesquisar por nome de filme"
        value={filterData?.search}
        onChange={(e) => {
          setFilterData({
            ...filterData,
            search: e.target.value,
            requested: true,
          });
        }}
      />
      <Button
        type="submit"
        variant="rounded"
        onClick={() => {
          navigate('/search');
          setFilterData({
            ...filterData,
            requested: true,
          });
        }}
      >
        Pesquisar
      </Button>
      <FilterModal />
    </div>
  );
};

export default SearchBar;
