import React from 'react';

import RegisterMovieModal from '../RegisterMovieModal';
import { IMovie } from '@/utils/interfaces/movie';

interface MoviesTableProps {
  movies: IMovie[];
  revalidate?: boolean;
  setRevalidate?: React.Dispatch<React.SetStateAction<boolean>>;
}

const MoviesTable: React.FC<MoviesTableProps> = ({
  movies,
  revalidate,
  setRevalidate,
}) => {
  return (
    <table className="w-full">
      <thead>
        <tr>
          <th className="p-2">Banner</th>
          <th className="p-2">Titulo</th>
          <th className="p-2">Diretor</th>
          <th className="p-2">Ações</th>
        </tr>
      </thead>
      <tbody>
        {movies.map((movie) => (
          <tr key={movie.id} className="border-b text-center">
            <td className="p-2 border-r-2">
              <img
                src={
                  movie.image ??
                  'https://avatars.githubmoviecontent.com/u/60272954?v=4'
                }
                alt="avatar"
                className="rounded-md w-10 h-10 mx-auto"
                onError={(e) => {
                  e.currentTarget.src =
                    'https://avatars.githubmoviecontent.com/u/60272954?v=4';
                }}
              />
            </td>
            <td className="p-2 border-r-2">{movie.title}</td>
            <td className="p-2 border-r-2">{movie.director}</td>
            <td className="p-2">
              <RegisterMovieModal
                isEdit
                movie={movie}
                revalidate={revalidate}
                setRevalidate={setRevalidate}
              />
            </td>
          </tr>
        ))}
      </tbody>
    </table>
  );
};

export default MoviesTable;
