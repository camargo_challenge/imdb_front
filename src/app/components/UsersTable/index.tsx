import React from 'react';
import RegisterUserModal from '../RegisterUserModal';
import { IUser } from '@/utils/interfaces/user';
import DisableUserModal from '../DeleteUserModal';

interface UsersTableProps {
  users: IUser[];
  revalidate: boolean;
  setRevalidate: React.Dispatch<React.SetStateAction<boolean>>;
}

const UsersTable: React.FC<UsersTableProps> = ({
  users,
  setRevalidate,
  revalidate,
}) => {
  return (
    <table className="w-full">
      <thead>
        <tr>
          <th className="p-2">Avatar</th>
          <th className="p-2">Nome</th>
          <th className="p-2">Email</th>
          <th className="p-2">Ações</th>
        </tr>
      </thead>
      <tbody>
        {users.map((user) => (
          <tr key={user.id} className="border-b text-center">
            <td className="p-2 border-r-2">
              <img
                src={
                  user.image ??
                  'https://avatars.githubusercontent.com/u/60272954?v=4'
                }
                alt="avatar"
                className="rounded-full w-10 h-10 mx-auto"
                onError={(e) => {
                  e.currentTarget.src =
                    'https://avatars.githubusercontent.com/u/60272954?v=4';
                }}
              />
            </td>
            <td className="p-2 border-r-2">{user.name}</td>
            <td className="p-2 border-r-2">{user.email}</td>
            <td className="p-2">
              <RegisterUserModal
                isEdit
                user={{ ...user, password: '', passwordConfirmation: '' }}
                revalidate={revalidate}
                setRevalidate={setRevalidate}
              />
              <DisableUserModal
                user={user}
                revalidate={revalidate}
                setRevalidate={setRevalidate}
              />
            </td>
          </tr>
        ))}
      </tbody>
    </table>
  );
};

export default UsersTable;
