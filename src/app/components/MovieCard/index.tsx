import { FC } from 'react';
import { Link } from 'react-router-dom';

interface MovieCardProps {
  title: string;
  banner: string;
  genreId: string;
  id: string;
}

const MovieCard: FC<MovieCardProps> = ({
  title,
  banner,
  genreId,
  id,
}: MovieCardProps) => {
  return (
    <Link
      id={id}
      to={`/${encodeURIComponent(genreId)}/${encodeURIComponent(id)}`}
      className="hover:scale-105 transition-all duration-300 cursor-pointer w-[250px] h-[300px] overflow-hidden min-w-[180px]"
    >
      <div
        className={`w-full h-[90%] bg-cover bg-center rounded-md`}
        style={{ backgroundImage: `url(${banner})` }}
      >
        {' '}
      </div>
      <h3 className="mt-2 text-base font-light text-center">{title}</h3>
    </Link>
  );
};

export default MovieCard;
