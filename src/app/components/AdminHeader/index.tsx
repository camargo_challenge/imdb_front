import React from 'react';
import Container from '../ui/container';
import { Link } from 'react-router-dom';

// import { Container } from './styles';

const AdminHeader: React.FC = () => {
  const actualUrl = window.location.href.split('/');
  const options = [
    {
      title: 'Users',
      url: 'admin',
    },
    {
      title: 'Movies',
      url: 'movies',
    },
  ];

  return (
    <header className="bg-[#ccccccd8] bg-opacity-50 backdrop-filter backdrop-blur-lg w-full z-[1]">
      <Container
        className="flex justify-around items-center py-2"
        variant="noPadding"
      >
        {options.map((option, i) => (
          <Link
            to={
              option.url.includes('admin')
                ? `/${option.url}`
                : `/admin/${option.url}`
            }
            key={i}
            className={`${
              actualUrl[actualUrl?.length - 1] === option?.url
                ? 'text-red-500'
                : 'text-black'
            }`}
          >
            <h6 className="text-lg font-bold">{option.title}</h6>
          </Link>
        ))}
      </Container>
    </header>
  );
};

export default AdminHeader;
