import { useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import React from 'react';

import MovieList from '@/app/components/MovieList';
import SearchBar from '@/app/components/SearchBar';
import Container from '@/app/components/ui/container';
import PageTitle from '@/app/components/ui/page-title';
import { api } from '@/services/api';
import { IGenre } from '@/utils/interfaces/genre';
import { IMovie } from '@/utils/interfaces/movie';
import FilterContext from '@/context/FilterContext';

const List = () => {
  const { category } = useParams<{ category: string }>();
  const { filterData, setFilterData } = React.useContext(FilterContext);
  const [genre, setGenre] = React.useState<IGenre>();
  const [movies, setMovies] = React.useState<IMovie[]>([]);
  const handleSearch = async () => {
    const genres = filterData?.genres?.map((genre) => `genres=${genre.value}`);
    const directors = filterData?.directors?.map(
      (director) => `directors=${director.value}`
    );
    const actors = filterData?.actors?.map((actor) => `actors=${actor.value}&`);
    const search = filterData?.search ? `search=${filterData?.search}` : '';

    const response = await api.get(
      `/movies?${genres.join('&')}${
        genres.length > 0 ? '&' : ''
      }${directors.join('&')}${directors.length > 0 ? '&' : ''}${actors.join(
        '&'
      )}${actors.length > 0 ? '&' : ''}${search}`
    );
    setFilterData({
      ...filterData,
      requested: false,
    });

    return response.data?.data as IMovie[];
  };

  React.useEffect(() => {
    if (category) {
      const fetchMovies = async () => {
        const response = await api.get(`/genres/${category}`);
        return response.data as IGenre;
      };

      fetchMovies()
        .then((data) => {
          setGenre(data);
          setMovies(data?.movies);
        })
        .catch((err) => {
          toast.error(err.response.data.message ?? 'Erro ao buscar filmes');
        });
    } else {
      handleSearch()
        .then((data) => {
          setMovies(data);
          setGenre(data[0]?.genre);
        })
        .catch((err) => {
          toast.error(err.response.data.message ?? 'Erro ao buscar filmes');
        });
    }
  }, []);

  React.useEffect(() => {
    handleSearch()
      .then((data) => {
        setMovies(data);
      })
      .catch((err) => {
        toast.error(err.response.data.message ?? 'Erro ao buscar filmes');
      });
  }, [filterData?.requested]);

  return (
    <Container className="flex flex-col gap-2">
      <SearchBar />
      <PageTitle title="Lista de Filmes" />
      <MovieList data={movies} genreId={genre?.id ?? ''} />
    </Container>
  );
};

export default List;
