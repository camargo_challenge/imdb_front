import AdminHeader from '@/app/components/AdminHeader';
import MoviesTable from '@/app/components/MoviesTable';
import RegisterMovieModal from '@/app/components/RegisterMovieModal';
import Container from '@/app/components/ui/container';
import TablePagination from '@/app/components/ui/pagination';
import { api } from '@/services/api';
import { IMovie } from '@/utils/interfaces/movie';

import React from 'react';
import { toast } from 'react-toastify';

const Movies: React.FC = () => {
  const [page, setPage] = React.useState(0);
  const [data, setData] = React.useState<IMovie[]>([]);
  const [totalPages, setTotalPages] = React.useState(0);
  const [revalidate, setRevalidate] = React.useState(false);

  React.useEffect(() => {
    const fetchMovies = async () => {
      const response = await api.get(`/movies?page=${page + 1}`);

      return response.data;
    };

    fetchMovies()
      .then((data) => {
        setData(
          data?.data.map((movie: IMovie) => ({
            ...movie,
            actors: movie.actors.map((actor) => ({
              label: actor.name,
              value: actor.id,
            })),
            genre: {
              label: movie.genre.name,
              value: movie.genre.id,
            },
            releaseDate: new Date(movie.releaseDate)
              .toISOString()
              .split('T')[0],
          }))
        );
        setTotalPages(data.pages);
      })
      .catch((err) => {
        toast.error(err.response?.data.message ?? 'Erro ao buscar filmes');
      });
  }, [page, revalidate]);

  return (
    <>
      <AdminHeader />
      <Container className="flex flex-col items-center justify-center">
        <div className="rounded-lg p-4 border bg-white w-full">
          <div className="my-4 flex justify-end mr-4">
            <RegisterMovieModal
              revalidate={revalidate}
              setRevalidate={setRevalidate}
            />
          </div>
          <MoviesTable
            movies={data}
            revalidate={revalidate}
            setRevalidate={setRevalidate}
          />
          <TablePagination
            page={page}
            setPage={setPage}
            totalPages={totalPages}
          />
        </div>
      </Container>
    </>
  );
};

export default Movies;
