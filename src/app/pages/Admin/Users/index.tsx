import AdminHeader from '@/app/components/AdminHeader';
import RegisterUserModal from '@/app/components/RegisterUserModal';
import UsersTable from '@/app/components/UsersTable';
import Container from '@/app/components/ui/container';
import TablePagination from '@/app/components/ui/pagination';
import { api } from '@/services/api';
import { IUser } from '@/utils/interfaces/user';

import React from 'react';
import { toast } from 'react-toastify';

const Users: React.FC = () => {
  const [page, setPage] = React.useState(0);
  const [totalPages, setTotalPages] = React.useState(0);
  const [users, setUsers] = React.useState<IUser[]>([]);
  const [revalidate, setRevalidate] = React.useState(false);

  React.useEffect(() => {
    const fetchUser = async () => {
      const response = await api.get(`/users?page=${page + 1}`);

      return response.data;
    };

    fetchUser()
      .then((data) => {
        setUsers(data.data);
        setTotalPages(data.pages);
      })
      .catch((err) => {
        toast.error(err.response.data.message ?? 'Erro ao buscar usuários');
      });
  }, [page, revalidate]);

  return (
    <>
      <AdminHeader />
      <Container className="flex flex-col items-center justify-center">
        <div className="rounded-lg p-4 border bg-white w-full">
          <div className="my-4 flex justify-end mr-4">
            <RegisterUserModal
              setRevalidate={setRevalidate}
              revalidate={revalidate}
            />
          </div>
          <UsersTable
            users={users}
            setRevalidate={setRevalidate}
            revalidate={revalidate}
          />
          <TablePagination
            page={page}
            setPage={setPage}
            totalPages={totalPages}
          />
        </div>
      </Container>
    </>
  );
};

export default Users;
