import Container from '@/app/components/ui/container';
import MovieSection from '../../components/MovieSection';
import SearchBar from '@/app/components/SearchBar';
import { api } from '@/services/api';
import React from 'react';
import { toast } from 'react-toastify';
import { IGenre } from '@/utils/interfaces/genre';

const Home = () => {
  const [genres, setGenres] = React.useState<IGenre[]>([]);

  React.useEffect(() => {
    const fetchMovies = async () => {
      const response = await api.get('/genres?onlyWithMovies=true');
      return response.data as IGenre[];
    };

    fetchMovies()
      .then((data) => {
        setGenres(data);
      })
      .catch((err) => {
        toast.error(err.response.data.message);
      });
  }, []);

  return (
    <Container>
      <SearchBar />
      {genres.map((genre) => (
        <MovieSection genre={genre} />
      ))}
    </Container>
  );
};

export default Home;
