import { fadeDown, staggerContainer } from '@/utils/motion';
import MovieSection from '../../components/MovieSection';
import { motion } from 'framer-motion';
import Genre from './Genre';
import ActorsSection from '@/app/components/ActorsSection';
import Poster from './Poster';
import Container from '@/app/components/ui/container';
import React from 'react';
import { IMovie } from '@/utils/interfaces/movie';
import { api } from '@/services/api';
import { useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import { IGenre } from '@/utils/interfaces/genre';
import RateMovieModal from '@/app/components/RateMovieModal';

const Detail = () => {
  const { id } = useParams();
  const [revalidate, setRevalidate] = React.useState(false);
  const [movie, setMovie] = React.useState<IMovie>();
  const [genre, setGenre] = React.useState<IGenre>();

  React.useEffect(() => {
    const fetchMovie = async () => {
      const response = await api.get(`/movies/${id}`);
      return response.data as IMovie;
    };

    fetchMovie()
      .then((data) => {
        setMovie(data);
      })
      .catch((err) => {
        toast.error(err.response.data.message ?? 'Erro ao buscar filme');
      });
  }, [revalidate]);

  React.useEffect(() => {
    if (movie) {
      const fetchGenre = async () => {
        const response = await api.get(`/genres/${movie.genre.id}`);
        return response.data as IGenre;
      };

      fetchGenre()
        .then((data) => {
          setGenre(data);
        })
        .catch((err) => {
          toast.error(
            err.response.data.message ?? 'Erro ao buscar filmes da categoria'
          );
        });
    }
  }, [movie]);

  return (
    <>
      <section
        className="w-full"
        style={{
          backgroundImage: `linear-gradient(to top, rgba(0,0,0), rgba(0,0,0,0.98),rgba(0,0,0,0.8) ,rgba(0,0,0,0.4)),url(${movie?.image})`,
          backgroundPosition: 'top',
          backgroundSize: 'cover',
        }}
      >
        <div
          className={`max-w-[97vw] flex-col sm:max-w-[80vw] lg:py-36 sm:py-[136px] sm:pb-28 xs:py-28 xs:pb-12 pt-24 pb-8 flex sm:flex-row lg:gap-12 md:gap-10 gap-8 justify-center items-center `}
        >
          {movie?.title && movie?.image && (
            <Poster title={movie?.title} banner={movie?.image} />
          )}
          <motion.div
            variants={staggerContainer(0.2, 0.4)}
            initial="hidden"
            animate="show"
            className="text-gray-300 sm:max-w-[80vw] max-w-[90vw]  md:max-w-[520px] font-nunito flex flex-col lg:gap-5 sm:gap-4 xs:gap-[14px] gap-3 mb-8 flex-1"
          >
            <motion.div variants={fadeDown} className="flex flex-col gap-4">
              <h2
                className={`sm:text-4xl xs:text-3xl text-[28.75px] font-extrabold sm:leading-tight xs:leading-normal leading-snug text-secColor sm:max-w-[420px] xs:max-w-[320px] max-w-[280px] md:max-w-[420px]`}
              >
                {movie?.title || ''}
              </h2>

              <div className="flex flex-row items-center md:gap-4 sm:gap-[14px] xs:gap-3 gap-[6px] flex-wrap">
                {movie?.genre && (
                  <Genre
                    key={movie?.genre?.id}
                    name={movie?.genre?.name ?? ''}
                    id={movie?.genre?.id ?? ''}
                  />
                )}
              </div>

              <p className="sm:text-base xs:text-[15.75px] text-[14.25px] leading-relaxed">
                <span>{movie?.synopsis}</span>
              </p>

              <h3 className="text-secColor font-bold md:text-[18px] sm:text-[16.75px] xs:text-[15.75px] text-[14.75px]">
                Atores
              </h3>

              {movie?.actors && <ActorsSection actors={movie?.actors} />}
              <h3 className="text-secColor font-bold md:text-[18px] sm:text-[16.75px] xs:text-[15.75px] text-[14.75px]">
                Avaliação - {movie?.avarageRate ?? 0}
              </h3>
              <div className="flex flex-row items-center md:gap-4 sm:gap-[14px] xs:gap-3 gap-[6px] flex-wrap">
                <RateMovieModal
                  movie={movie}
                  revalidate={revalidate}
                  setRevalidate={setRevalidate}
                />
              </div>
            </motion.div>
          </motion.div>
        </div>
      </section>
      <Container className="mt-12">
        {genre && <MovieSection genre={genre} />}
      </Container>
    </>
  );
};

export default Detail;
