interface PosterProps {
  title: string;
  banner: string;
}

const Poster = ({ title, banner }: PosterProps) => {
  return (
    <div className="relative">
      <img
        src={banner}
        alt={title}
        className="rounded-md max-h-[250px] min-h-[216px]"
      />
      <div className="absolute inset-0 bg-gradient-to-t from-black to-transparent"></div>
    </div>
  );
};
export default Poster;
