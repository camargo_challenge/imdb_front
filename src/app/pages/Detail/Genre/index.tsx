import CircularBadge from '@/app/components/ui/circular-badge';
import { Link } from 'react-router-dom';

const Genre = ({ name, id }: { name: string; id: string }) => {
  return (
    <CircularBadge
      as={Link}
      className="text-white bg-transparent border-white hover:bg-gray-10"
      to={`/${encodeURIComponent(id)}`}
    >
      {name}
    </CircularBadge>
  );
};

export default Genre;
