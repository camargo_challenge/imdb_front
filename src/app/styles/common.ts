export const selectStyles = {
  control: (provided: any) => ({
    ...provided,
    border: '1px solid #000',
    borderRadius: '9999px',
  }),
};
