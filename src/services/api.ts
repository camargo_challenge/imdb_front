import axios from 'axios';
const URL = import.meta.env.VITE_API_URL;

export const api = axios.create({
  baseURL: URL,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
});

api.interceptors.request.use(async (config: any) => {
  const token = getCookies('userData')?.token ?? '';

  if (token) config.headers.Authorization = 'Bearer ' + token;
  return config;
});

api.interceptors.response.use(
  (response) => {
    return response;
  },
  async function (error: any) {
    const originalRequest = error.config;

    if (
      error?.status === 401 &&
      !originalRequest._retry &&
      originalRequest.url !== '/auth/refresh-token' &&
      originalRequest.url !== '/auth/login'
    ) {
      originalRequest._retry = true;

      const { token } = await getRefreshToken(
        getCookies('userData')?.refreshToken
      ).catch(() => (window.location.href = '/'));

      api.defaults.headers.common['Authorization'] = `Bearer ${token}`;
      return api(originalRequest);
    }
    return Promise.reject(error);
  }
);

const getRefreshToken = async (refreshToken: string) => {
  try {
    const resp = await api.post('/auth/refresh-token', {
      refreshToken,
    });

    console.log('Reautenticando pelo interceptor');

    return resp?.data;
  } catch (e) {
    console.log('Error', e);
  }
};

const getCookies = (key: string) => {
  const cookies = document.cookie.split(';');
  const cookie = cookies.find((cookie) => cookie.includes(key));

  if (!cookie) return null;

  const decodedCookieValue = decodeURIComponent(cookie);
  const splittedCookie = decodedCookieValue.split('=');
  const cookieObject = JSON.parse(splittedCookie[1]);

  return cookieObject;
};
