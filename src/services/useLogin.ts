import { useContext } from 'react';

import { IUser } from '@/utils/interfaces/user';
import { api } from './api';
import UserContext, { UserContextProps } from '@/context/UserContext';

interface LoginInputProps {
  email: string;
  password: string;
}

export interface LoginResponseProps {
  token: string;
  refreshToken: string;
  user: IUser;
}

const useLogin = async (loginInput: LoginInputProps) => {
  const { setUserData } = useContext(UserContext) as UserContextProps;

  const response = await api.post('/auth/login', loginInput);

  setUserData(response.data);

  return response.data;
};

export default useLogin;
