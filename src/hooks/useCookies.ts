import { useState } from 'react';
import { useCookies as useReactCookies } from 'react-cookie';

export default function useCookies(key: any, initialValue: any) {
  const [cookies, setCookie] = useReactCookies([key]);
  const [storedValue, setStoredValue] = useState(() => {
    try {
      const item = cookies[key];
      return item ? item : initialValue;
    } catch (error) {
      /* eslint-disable-next-line no-console */
      console.log(error);
      return initialValue;
    }
  });

  const setValue = (value: any) => {
    try {
      const valueToStore =
        value instanceof Function ? value(storedValue) : value;
      setStoredValue(valueToStore);
      setCookie(key, JSON.stringify(valueToStore), { path: '/', secure: true });
    } catch (error) {
      /* eslint-disable-next-line no-console */
      console.log(error);
    }
  };

  return [storedValue, setValue];
}
