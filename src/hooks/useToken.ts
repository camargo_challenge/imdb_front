import { useContext } from 'react';

import UserContext from '@/context/UserContext';
import { UserContextProps } from '@/context/UserContext';
import useCookies from './useCookies';

export default function useUserData() {
  const [userData] = useCookies('userData', null);

  const { userData: user } =
    (useContext(UserContext) as UserContextProps) ?? userData;
  return user;
}
