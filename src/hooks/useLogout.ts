import { useContext } from 'react';

import UserContext from '@/context/UserContext';
import { UserContextProps } from '@/context/UserContext';
import useCookies from './useCookies';

export default function useLogout() {
  const [, setUserData] = useCookies('userData', null);

  const { setUserData: setUser } = useContext(UserContext) as UserContextProps;

  function logout() {
    setUserData(null);
    setUser({
      token: null,
      refreshToken: null,
      user: null,
    });
  }

  return logout;
}
