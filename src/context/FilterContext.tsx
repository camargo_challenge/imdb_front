import React, { createContext } from 'react';

export interface SelectOption {
  value: string;
  label: string;
  __isNew__?: boolean;
}

export interface FilterContextProps {
  filterData: {
    genres: any[];
    directors: any[];
    actors: any[];
    search: string;
    requested: boolean;
  };
  setFilterData: (data: any) => void;
}

const FilterContext = createContext({} as FilterContextProps);

export default FilterContext;

export function FilterProvider({ children }: { children: React.ReactNode }) {
  const [filterData, setFilterData] = React.useState({
    genres: [],
    directors: [],
    actors: [],
    search: '',
    requested: false,
  });
  const memoizedFilterData = React.useMemo(
    () => ({ filterData, setFilterData }),
    [filterData, setFilterData]
  );

  return (
    <FilterContext.Provider value={memoizedFilterData}>
      {children}
    </FilterContext.Provider>
  );
}
