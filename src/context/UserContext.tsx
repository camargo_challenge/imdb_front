import { createContext } from 'react';

import useLocalStorage from '../hooks/useCookies';
import { IUser } from '@/utils/interfaces/user';

interface UserDataProps {
  token: string | null;
  user: IUser | null;
  refreshToken: string | null;
}

export interface UserContextProps {
  userData: UserDataProps;
  setUserData: (data: UserDataProps) => void;
}

const UserContext = createContext({} as UserContextProps);

export default UserContext;

export function UserProvider({ children }: { children: React.ReactNode }) {
  const [userData, setUserData] = useLocalStorage('userData', {});

  return (
    <UserContext.Provider value={{ userData, setUserData }}>
      {children}
    </UserContext.Provider>
  );
}
