import { Route, Routes } from 'react-router';
import { Navigate } from 'react-router-dom';
import React, { Suspense } from 'react';
import { toast } from 'react-toastify';

import Header from './app/components/Header';
import ScrollToTop from './app/common/ScrollTop';
import { UserProvider } from './context/UserContext';
import useUserData from './hooks/useToken';
import { UserRoleEnum } from './utils/interfaces/user';
import Loading from './app/components/Loading';
import { FilterProvider } from './context/FilterContext';

const Home = React.lazy(() => import('./app/pages/Home/index'));
const List = React.lazy(() => import('./app/pages/List/index'));
const Detail = React.lazy(() => import('./app/pages/Detail/index'));
const Users = React.lazy(() => import('./app/pages/Admin/Users'));
const Movies = React.lazy(() => import('./app/pages/Admin/Movies'));

function App() {
  return (
    <UserProvider>
      <FilterProvider>
        <Header />
        <main className="lg:pb-14 md:pb-4 sm:pb-2 xs:pb-1 pb-0 font-roboto bg-[#fafafa] pt-24 min-h-screen">
          <ScrollToTop>
            <Suspense fallback={<Loading />}>
              <Routes>
                <Route path="/" element={<Home />} />
                <Route path="/:category" element={<List />} />
                <Route path="/search" element={<List />} />
                <Route path="/:category/:id" element={<Detail />} />
                <Route
                  path="/admin"
                  element={
                    <ProtectedRouteGuard>
                      <Users />
                    </ProtectedRouteGuard>
                  }
                />
                <Route
                  path="/admin/movies"
                  element={
                    <ProtectedRouteGuard>
                      <Movies />
                    </ProtectedRouteGuard>
                  }
                />
              </Routes>
            </Suspense>
          </ScrollToTop>
        </main>
      </FilterProvider>
    </UserProvider>
  );
}

function ProtectedRouteGuard({ children }: { children: React.ReactNode }) {
  const userData = useUserData();

  if (!userData?.token) {
    toast.error('Você precisa estar logado para acessar esta página');
    return <Navigate to="/" />;
  }

  if (
    userData?.user?.role !== UserRoleEnum['ADMIN'] &&
    userData?.user?.role !== UserRoleEnum['MASTER']
  ) {
    toast.error('Você não tem permissão para acessar esta página');
    return <Navigate to="/" />;
  }

  return <>{children}</>;
}

export default App;
