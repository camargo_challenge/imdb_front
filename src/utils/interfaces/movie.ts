import { IActor } from './actors';
import { IGenre } from './genre';
import { IRating } from './rating';

export enum MovieClassificationEnum {
  LIVRE = 'Livre',
  DEZ = '10 anos',
  DOZE = '12 anos',
  QUATORZE = '14 anos',
  DEZESSEIS = '16 anos',
  DEZOITO = '18 anos',
}

export enum MovieStatusEnum {
  ATIVO = 'ativo',
  INATIVO = 'inativo',
}

export interface IMovie {
  id: string;
  createdAt: Date;
  updatedAt: Date;
  deletedAt: Date;
  title: string;
  director: string;
  releaseDate: Date;
  image: string;
  imageKey: string;
  synopsis: string;
  duration: string;
  classification: MovieClassificationEnum;
  trailer: string;
  status: MovieStatusEnum;
  genre: IGenre;
  actors: IActor[];
  avarageRate: number;
  ratings: IRating[];
}
