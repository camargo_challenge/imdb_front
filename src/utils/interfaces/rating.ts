import { IMovie } from './movie';
import { IUser } from './user';

export interface IRating {
  id: string;
  createdAt: Date;
  updatedAt: Date;
  deletedAt: Date;
  rate: number;
  user: IUser;
  movie: IMovie;
}
