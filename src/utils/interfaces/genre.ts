import { IMovie } from './movie';

export interface IGenre {
  id: string;
  createdAt: Date;
  updatedAt: Date;
  deletedAt: Date;
  name: string;
  movies: IMovie[];
  value?: string;
}
