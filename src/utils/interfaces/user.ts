export enum UserRoleEnum {
  'USER' = 'USER',
  'ADMIN' = 'ADMIN',
  'MASTER' = 'MASTER',
}

export enum UserStatusEnum {
  'ativo' = 'ativo',
  'inativo' = 'inativo',
}

export interface IUser {
  id: string;
  name: string;
  role: UserRoleEnum;
  email: string;
  status: UserStatusEnum;
  image: string;
  imageKey: string;
  createdAt: Date;
  updatedAt: Date;
  deletedAt: Date;
}
