export interface IActor {
  id: string;
  createdAt: Date;
  updatedAt: Date;
  deletedAt: Date;
  name: string;
  value: string;
  label: string;
}
