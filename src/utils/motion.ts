const isMobile = window.innerWidth < 768;

export const fadeDown = isMobile
  ? undefined
  : {
      hidden: {
        opacity: 0,
        y: -25,
      },
      show: {
        opacity: 1,
        y: 0,
        transition: {
          duration: 0.6,
          ease: 'easeInOut',
          type: 'tween',
        },
      },
    };

export const staggerContainer = (
  staggerChildren: number,
  delayChildren: number
) =>
  isMobile
    ? undefined
    : {
        hidden: {
          opacity: 0,
        },
        show: {
          opacity: 1,
          transition: {
            staggerChildren,
            delayChildren,
          },
        },
      };
