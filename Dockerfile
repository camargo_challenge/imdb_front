FROM node:18.16.0

WORKDIR /home/app
COPY ./package*.json ./

RUN npm install
COPY . .

EXPOSE 5173

RUN npm run build
